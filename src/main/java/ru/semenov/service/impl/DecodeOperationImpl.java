package ru.semenov.service.impl;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import ru.semenov.service.DecodeOperation;
import ru.semenov.validator.ValidatorEmailAndCode;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@ApplicationScoped
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DecodeOperationImpl implements DecodeOperation<String> {
    ValidatorEmailAndCode validator;

    @Override
    public Uni<String> decode(String source) {
        return Uni.createFrom()
                .nullItem()
                .flatMap(ignored -> validator.validateSource(source))
                .flatMap(valid -> decodeEmailWithCode(source));
    }

    @SneakyThrows
    private Uni<String> decodeEmailWithCode(String valid) {
        return Uni.createFrom()
                .item(valid)
                .onItem()
                .transform(var -> new String(Base64
                        .getDecoder()
                        .decode(var), StandardCharsets.UTF_8));
    }
}
