package ru.semenov.validator.impl;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import ru.semenov.validator.ValidatorEmailAndCode;

import static java.util.Objects.isNull;

@ApplicationScoped
public class ValidatorEmailAndCodeImpl implements ValidatorEmailAndCode {
    @Override
    public Uni<Void> validateEmailAndCode(String email, String code) {
        return isNull(email) || isNull(code)
                ? Uni.createFrom().failure(IllegalArgumentException::new)
                : Uni.createFrom().voidItem();
    }

    @Override
    public Uni<Void> validateSource(String source) {
        return Uni.createFrom()
                .voidItem()
                .flatMap(ignored -> validateSourceIsEmpty(source));
    }


    private Uni<Void> validateSourceIsEmpty(String source) {
        return isNull(source) || source.isEmpty()
                ? Uni.createFrom().failure(IllegalArgumentException::new)
                : Uni.createFrom().voidItem();
    }
}
