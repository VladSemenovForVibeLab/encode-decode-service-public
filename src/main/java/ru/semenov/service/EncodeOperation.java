package ru.semenov.service;

import io.smallrye.mutiny.Uni;

public interface EncodeOperation<T> {
    Uni<T> encode(T email, T code);
    T encodeLock(T email, T code);
    T encodeWithReentrantLock (T email, T code);
}
