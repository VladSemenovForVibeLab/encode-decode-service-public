package ru.semenov.controller;

import io.smallrye.mutiny.Uni;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import ru.semenov.service.DecodeOperation;
import ru.semenov.service.EncodeOperation;

@Path("/api")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class EncodeController {
    EncodeOperation<String> encodeOperation;
    DecodeOperation<String> decodeOperation;
    @GET
    @Path("/encode")
    public Uni<String> getEncode(@QueryParam("email") @Email String email,
                                 @QueryParam("code") String code) {

        return Uni.createFrom()
                .nullItem()
                .flatMap(ignored -> encodeOperation.encode(email,code));
    }
    @POST
    @Path("/decode")
    public Uni<String> decode(@Valid @QueryParam("email") String source) {
        return Uni.createFrom()
                .nullItem()
                .flatMap(ignored -> decodeOperation.decode(source));
    }
    @GET
    @Path("/encodeLock")
    public String getEncodeLock(@QueryParam("email") @Email String email,
                                 @QueryParam("code") String code) {

        return encodeOperation.encodeLock(email,code);
    }
    @GET
    @Path("/encodeReentrantLock")
    public String getEncodeReentrantLock(@QueryParam("email") @Email String email,
                                @QueryParam("code") String code) {
        return encodeOperation.encodeWithReentrantLock(email,code);
    }
}
