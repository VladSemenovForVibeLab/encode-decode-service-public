package ru.semenov.validator;

import io.smallrye.mutiny.Uni;

public interface ValidatorEmailAndCode {

    Uni<Void> validateEmailAndCode(String email, String code);

    Uni<Void> validateSource(String source);
}
