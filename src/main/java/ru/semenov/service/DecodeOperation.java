package ru.semenov.service;

import io.smallrye.mutiny.Uni;

public interface DecodeOperation<T> {
    Uni<T> decode(T source);
}
