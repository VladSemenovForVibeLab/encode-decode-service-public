package ru.semenov.service.impl;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import ru.semenov.service.EncodeOperation;
import ru.semenov.validator.ValidatorEmailAndCode;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.concurrent.locks.ReentrantLock;


@ApplicationScoped
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class EncodeOperationImpl implements EncodeOperation<String> {
    ValidatorEmailAndCode validator;
    Object lock = new Object();
    ReentrantLock reentrantLock = new ReentrantLock();

    @Override
    public Uni<String> encode(String email, String code) {
        return Uni.createFrom()
                .nullItem()
                .flatMap(ignored -> validator.validateEmailAndCode(email, code))
                .flatMap(valid -> encodeEmailWithCode(email, code));
    }

    @Override
    public String encodeLock(String email, String code) {
        validator.validateEmailAndCode(email, code);
        synchronized (lock) {
            String data = email +
                    ":" +
                    code;
            return Base64.getEncoder()
                    .encodeToString(data
                            .getBytes());
        }
    }

    @Override
    public String encodeWithReentrantLock(String email, String code) {
       try{
           validator.validateEmailAndCode(email, code);
           reentrantLock.lock();
           String data = email +
                   ":" +
                   code;
           return Base64.getEncoder()
                   .encodeToString(data
                           .getBytes());
       }finally {
           reentrantLock.unlock();
       }
    }

    @SneakyThrows
    private Uni<String> encodeEmailWithCode(String email, String code) {
        return Uni.createFrom()
                .item(email + ":" + code)
                .onItem()
                .transform(item -> Base64
                        .getEncoder()
                        .encodeToString(item.getBytes(StandardCharsets.UTF_8)));
    }
}
